from pathlib import Path


class Piece:
    def __init__(self, owner, piece, coord):
        self.owner = owner
        self.piece = piece
        self.coord = coord


class Board:
    def __init__(self, s):
        """Create a board from a string
           string is comma separated list of pieces CC13 etc
        """
        a = s.split(',')
        self.pieces = []
        for p in a:
            owner = p[0]
            piece = p[1]
            coord = p[2:]
            piece = Piece(owner, piece, coord)
            self.pieces.append(piece)

    def _repr_svg_(self):
        svg_path = Path(__file__).parent / 'svg'
        with open(svg_path / 'JanggiWood.svg', 'r') as fp:
            board_svg = fp.read()[:-6]
        for p in self.pieces:
            if p.owner == 'C':
                owner = 'blue'
            else:
                owner = 'red'
            if p.piece == 'R':
                piece = 'chariot'
            elif p.piece == 'C':
                piece = 'cannon'
            elif p.piece == 'H':
                piece = 'horse'
            elif p.piece == 'E':
                piece = 'elephant'
            elif p.piece == 'P':
                piece = 'pawn'
            elif p.piece == 'O':
                piece = 'advisor'
            else: 
                piece = 'king'
            filename = f'svg/{owner}_{piece}.svg'

            c = int(p.coord)
            if c in range(1, 11):
                x = 0
            elif c in range(11, 21):
                x = 1
            elif c in range(21, 31):
                x = 2
            elif c in range(31, 41):
                x = 3
            elif c in range(41, 51):
                x = 4
            elif c in range(51, 61):
                x = 5
            elif c in range(61, 71):
                x = 6
            elif c in range(71, 81):
                x = 7
            elif c in range(81, 91):
                x = 8
            if c in {1, 11, 21, 31, 41, 51, 61, 71, 81}:
                y = 0
            if c in {2, 12, 22, 32, 42, 52, 62, 72, 82}:
                y = 1
            if c in {3, 13, 23, 33, 43, 53, 63, 73, 83}:
                y = 2
            if c in {4, 14, 24, 34, 44, 54, 64, 74, 84}:
                y = 3
            if c in {5, 15, 25, 35, 45, 55, 65, 75, 85}:
                y = 4
            if c in {6, 16, 26, 36, 46, 56, 66, 76, 86}:
                y = 5
            if c in {7, 17, 27, 37, 47, 57, 67, 77, 87}:
                y = 6
            if c in {8, 18, 28, 38, 48, 58, 68, 78, 88}:
                y = 7
            if c in {9, 19, 29, 39, 49, 59, 69, 79, 89}:
                y = 8
            if c in {10, 20, 30, 40, 50, 60, 70, 80, 90}:
                y = 9
            image = f'<image xlink:href="{filename}" width="120" height="120" x="{x * 100 - 10}" y="{y * 100 - 10}"/>'
            board_svg += image
        return board_svg + '</svg>'
