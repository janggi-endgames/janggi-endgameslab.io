===========================
Longest immobility problems
===========================

.. list-table::

    * - Endgame
      - 漢
      - 楚
    * - :ref:`漢包馬楚包`
      - 5
      - NA
    * - :ref:`漢包馬士楚包`
      - 16
      - NA


.. _漢包馬楚包:

漢包馬楚包
~~~~~~~~~~~~

.. janggi-board::

    CK31,CC32,HK48,HC49,HH59

.. janggi-moves::

    1. HH59-67
    2. HH67-55
    3. HH55-63
    4. HH63-71
    5. HH71-52 mate


.. _漢包馬士楚包:

漢包馬士楚包
~~~~~~~~~~~~

.. janggi-board::

    HH51,CC42,CK33,HC34,HK38,HO39

.. janggi-moves::

    1. HO39-40
    2. HK38-39
    3. HO40-50
    4. HK39-40
    5. HO50-60
    6. HK40-50
    7. HO60-59
    8. HK50-60
    9. HC34-32
    10. HC32-40
    11. HC40-70
    12. HC70-60
    13. HO59-49
    14. HO49-39
    15. HC50-70
    16. HC70-40 mate
